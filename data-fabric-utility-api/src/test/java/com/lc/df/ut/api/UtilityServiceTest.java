package com.lc.df.ut.api;

import com.lc.df.error.model.DataFabricException;
import com.lc.df.ut.addr.service.AddressSearchService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilityServiceTest {

    private MockRestServiceServer utilityMockServer;

    @Autowired
    private TestBuilder testBuilder;


    @Autowired
    RestTemplate utilityRestTemplate;

    @Value("${uri.address.search.path}")
    private String addressSearchPath;

    @Value("${uri.address.search.key}")
    private String addressSearchKey;

    @Value("${uri.username.address.search}")
    private String addressSearchUsername;

    @Autowired
    private AddressSearchService addressSearchService;

    @Before
    public void setUp() throws Exception {
        utilityMockServer = MockRestServiceServer.createServer(utilityRestTemplate);
    }

    @Test
    public void testAddressSearchValidPostcode() throws Exception {

        String setAddressSearchResponse = testBuilder.buildAddressSearchSuccessResponse("11410384.00",
                " Blenheim Court 72 Horn Lane", "Woodford Green");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(addressSearchPath)
                .queryParam("Key", addressSearchKey)
                .queryParam("Postcode", "IG89AQ")
                .queryParam("UserName", addressSearchUsername);

        utilityMockServer.expect(requestTo(builder.toUriString())).
                andRespond(withSuccess(setAddressSearchResponse, MediaType.APPLICATION_XML));

        String result = addressSearchService.searchAddress("IG89AQ", "324323", "8324782");

        assertEquals(result, setAddressSearchResponse);

    }

    @Test(expected = DataFabricException.class)
    public void testAddressSearchInValidPostcode() throws Exception {

        String response = testBuilder.buildAddressSearchErrorResponse("1002",
                "Postcode Invalid", "The Postcode must be a full, valid UK postcode.",
                "Check the Postcode is valid and try again.");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(addressSearchPath)
                .queryParam("Key", addressSearchKey)
                .queryParam("Postcode", "IG8")
                .queryParam("UserName", addressSearchUsername);

        utilityMockServer.expect(requestTo(builder.toUriString())).
                andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        addressSearchService.searchAddress("IG8", "324323", "8324782");

    }

    @Test(expected = DataFabricException.class)
    public void testAddressSearchNoPostcode() throws Exception {

        String response = testBuilder.buildAddressSearchErrorResponse("1001",
                "Postcode Required", "The Postcode parameter was not supplied.",
                "Please ensure that you supply the Postcode parameter and try again.");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(addressSearchPath)
                .queryParam("Key", addressSearchKey)
                .queryParam("Postcode", "")
                .queryParam("UserName", addressSearchUsername);

        utilityMockServer.expect(requestTo(builder.toUriString())).
                andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

        addressSearchService.searchAddress("", "324323", "8324782");
    }

}