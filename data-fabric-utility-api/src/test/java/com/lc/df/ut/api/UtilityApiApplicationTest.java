package com.lc.df.ut.api;


import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilityApiApplicationTest {

    private static int records;

    private MockMvc apiMockServer;

    private MockRestServiceServer utilityMockServer;

    private static KafkaMessageListenerContainer<String, byte[]> container;

    @Autowired
    private TestBuilder testBuilder;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    RestTemplate utilityRestTemplate;

    @Value("${module.name}")
    private String moduleName;

    @Value("${uri.address.search.path}")
    private String addressSearchPath;

    @Value("${uri.address.search.key}")
    private String addressSearchKey;

    @Value("${uri.username.address.search}")
    private String addressSearchUsername;

    @ClassRule
    public static KafkaEmbedded embeddedKafka =
            new KafkaEmbedded(1, true, 1, "test_topic");

    @BeforeClass
    public static void setUpBeforeClass() {

        System.setProperty("kafka.servers.producer", embeddedKafka.getBrokersAsString());
        System.setProperty("kafka.topic.name", "test_topic");
        System.setProperty("kafka.send.event", "true");
        System.setProperty("module.include.ob", "true");

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, embeddedKafka.getBrokersAsString());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test_group");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        DefaultKafkaConsumerFactory<String, byte[]> consumerFactory =
                new DefaultKafkaConsumerFactory<String, byte[]>(props);

        ContainerProperties containerProperties = new ContainerProperties("test_topic");

        container = new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);
        container.setupMessageListener(new MessageListener<String, String>() {

            @Override
            public void onMessage(ConsumerRecord<String, String> record) {

                records++;
            }
        });

        container.start();
    }

    @Before
    public void setUp() throws Exception {

        apiMockServer = MockMvcBuilders.webAppContextSetup(context).build();
        utilityMockServer = MockRestServiceServer.createServer(utilityRestTemplate);
        records = 0;
    }

    @Test
    public void testHealthEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/ping"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status", is("UP")));
    }

    @Test
    public void testAuditEventsEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/auditevents"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.events").exists());
    }

    @Test
    public void testAutoConfigEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/autoconfig"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.positiveMatches").exists());
    }

    @Test
    public void testBeansEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/beans"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testConfigPropsEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/configprops"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDumpEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/dump"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testEnvEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/env"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.profiles").exists());
    }

    @Test
    public void testInfoEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/info"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testLoggersEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/loggers"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.loggers").exists());
    }

    @Test
    public void testMetricsEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/metrics"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.mem").exists());
    }

    @Test
    public void testMappingsEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/mappings"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testTraceEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/trace"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testHeapdumpEndpoint() throws Exception {

        apiMockServer.perform(MockMvcRequestBuilders.get("/heapdump"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testAddressSearchMissingHostHeader() throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("X-Client-Request-ID", "12345");
        map.put("X-Forwarded-For", "129.78.138.66");
        map.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

        HttpHeaders headers = testBuilder.buildRequestHeaders(map);
        String request = testBuilder.buildAddressSearchRequest("738792", "en-GB", "SE10 0DX");

        apiMockServer.perform(MockMvcRequestBuilders.get("/v4/utility-api/postcodes/SE16 8TF/addresses?locale=en-GB&api-key=738792")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(headers)
                .content(request)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }

    @Test
    public void testAddressSearchMissingXClientRequestIdHeader() throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("Host", "www.ladbrokescoral.com");
        map.put("X-Forwarded-For", "129.78.138.66");
        map.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

        HttpHeaders headers = testBuilder.buildRequestHeaders(map);
        String request = testBuilder.buildAddressSearchRequest("738792", "en-GB", "SE10 0DX");

        apiMockServer.perform(MockMvcRequestBuilders.get("/v4/utility-api/postcodes/SE16 8TF/addresses?locale=en-GB&api-key=738792")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(headers)
                .content(request)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }

    @Test
    public void testAddressSearchMissingXForwardedForHeader() throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("Host", "www.ladbrokescoral.com");
        map.put("X-Client-Request-ID", "12345");
        map.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");

        HttpHeaders headers = testBuilder.buildRequestHeaders(map);
        String request = testBuilder.buildAddressSearchRequest("738792", "en-GB", "SE10 0DX");

        apiMockServer.perform(MockMvcRequestBuilders.get("/v4/utility-api/postcodes/SE16 8TF/addresses?locale=en-GB&api-key=738792")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(headers)
                .content(request)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }

    @Test
    public void testAddressSearchMissingUserAgentHeader() throws Exception {

        HashMap<String, String> map = new HashMap<>();
        map.put("Host", "www.ladbrokescoral.com");
        map.put("X-Client-Request-ID", "12345");
        map.put("X-Forwarded-For", "129.78.138.66");

        HttpHeaders headers = testBuilder.buildRequestHeaders(map);
        String request = testBuilder.buildAddressSearchRequest("738792", "en-GB", "SE10 0DX");

        apiMockServer.perform(MockMvcRequestBuilders.get("/v4/utility-api/postcodes/SE16 8TF/addresses?locale=en-GB&api-key=738792")
                .contentType(MediaType.APPLICATION_JSON)
                .headers(headers)
                .content(request)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }

    @Test
    @Ignore
    public void testAddressSearchNoPostcode() throws Exception {

        HashMap<String, String> map = new HashMap<>();

        map.put("X-Message-Id", "345678");

        HttpHeaders headers = testBuilder.buildRequestHeaders(map);

        String addressSearchRequest = testBuilder.buildAddressSearchRequest("738792", "en-GB", null);

        String addressSearchResponse = testBuilder.
                buildAddressSearchErrorsResponse("MISSING-REQUIRED-PARAMETERS",
                        "One or more required parameters are missing. [postcode] ",
                        400);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(addressSearchPath)
                .queryParam("Key", addressSearchKey)
                .queryParam("Postcode", "")
                .queryParam("UserName", addressSearchUsername);

        utilityMockServer.expect(requestTo(builder.toUriString())).andRespond(withSuccess(addressSearchResponse , MediaType.APPLICATION_XML));

        MvcResult result = apiMockServer.perform(MockMvcRequestBuilders.get("/v4/utility-api/postcodes/SE16 8TF/addresses?locale=en-GB&api-key=738792").
                contentType(MediaType.APPLICATION_JSON).
                headers(headers).
                content(addressSearchRequest).
                accept(MediaType.APPLICATION_JSON)).
                andReturn();

        String actualResult = result.getResponse().getContentAsString().substring(12, result.getResponse().getContentAsString().length());

        JSONAssert.assertEquals(addressSearchResponse, actualResult , false);
    }

}