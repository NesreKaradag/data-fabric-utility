package com.lc.df.ut.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lc.df.ut.model.APIAddressSearchErrorResponse;
import com.lc.df.ut.model.APIAddressSearchRequest;
import com.lc.df.ut.model.APIAddressSearchResponse;
import com.lc.df.ut.model.APIAddressSearchSuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.util.HashMap;


@Component
class TestBuilder {

    @Autowired
    private ObjectMapper mapper;

    HttpHeaders buildRequestHeaders(HashMap<String, String> map) {
        HttpHeaders headers = new HttpHeaders();

        for (HashMap.Entry<String, String> entry : map.entrySet()) {
            headers.add(entry.getKey(), entry.getValue());
        }

        return headers;

    }

    private String jsonResponseWriter(Object response) throws JAXBException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(response);
    }

    String buildAddressSearchErrorsResponse(String errorCode, String errorDescription,
                                            int responseStatusCode) throws Exception {
        APIAddressSearchResponse addressSearchError = new APIAddressSearchResponse();
        addressSearchError.setErrorCode(errorCode);
        addressSearchError.setErrorDescription(errorDescription);
        addressSearchError.setResponseStatusCode(responseStatusCode);

        return jsonResponseWriter(addressSearchError);
    }

    String buildAddressSearchErrorResponse(String errorCode, String errorDescription,
                                           String errorCause, String errorResolution) throws Exception {
        APIAddressSearchErrorResponse addressSearchError = new APIAddressSearchErrorResponse();
        addressSearchError.setErrorCode(errorCode);
        addressSearchError.setErrorDescription(errorDescription);
        addressSearchError.setErrorCause(errorCause);
        addressSearchError.setErrorResolution(errorResolution);

        return jsonResponseWriter(addressSearchError);
    }

    String buildAddressSearchSuccessResponse(String id, String place, String streetAddress) throws Exception {
        APIAddressSearchSuccessResponse success = new APIAddressSearchSuccessResponse();
        success.setId(id);
        success.setPlace(place);
        success.setStreetAddress(streetAddress);

        return mapper.writeValueAsString(success);
    }

    String buildAddressSearchRequest(String apiKey, String locale, String postcode) throws Exception {
        APIAddressSearchRequest addressSearchRequest = new APIAddressSearchRequest();
        addressSearchRequest.setApiKey(apiKey);
        addressSearchRequest.setLocale(locale);
        addressSearchRequest.setPostcode(postcode);

        return mapper.writeValueAsString(addressSearchRequest);
    }
} 
