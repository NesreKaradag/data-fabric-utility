package com.lc.df.ut.api;

import com.lc.df.ut.model.APIAddressSearchErrorResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.util.HashMap;


@Configuration
public class TestConfig {

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(APIAddressSearchErrorResponse.class);

        marshaller.setMarshallerProperties(new HashMap<String, Object>() {
            private static final long serialVersionUID = 1L;

            {
                put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
            }
        });

        return marshaller;
    }
}