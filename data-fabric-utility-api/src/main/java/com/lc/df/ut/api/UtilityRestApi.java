package com.lc.df.ut.api;

import ch.qos.logback.classic.Level;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.ut.addr.service.AddressSearchService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;


@Api(value = "Utility API", description = "The Utility API provides REST endpoints" +
        " that allow clients to register a new customer.")
@RestController
@Validated
@RequestMapping("v4/utility-api")
public class UtilityRestApi {

    @Autowired
    AddressSearchService addressSearchService;

    @Autowired
    private DataFabricLogger logger;

    @Value("${module.name}")
    private String moduleName;

    @GetMapping(
            value = "/postcodes/{postcode}/addresses",
            produces = {MediaType.APPLICATION_JSON_VALUE, "application/json"}
    )
    public ResponseEntity<?> searchAddress(
            @PathVariable("postcode") String postcode,
            @RequestParam(value = "locale", required = false, defaultValue = "en-GB") String locale,
            @RequestParam(value = "api-key", required = false) String apiKey,
            @RequestHeader(value = "Host", required = false) String host,
            @RequestHeader(value = "User-Agent", required = false) String userAgent,
            @RequestHeader(value = "X-Client-Request-ID", required = false) String xClientRequestId,
            @RequestHeader(value = "X-Forwarded-For", required = false) String xForwardedFor,
            @RequestHeader(value = "X-Lc-Cid", required = false) String xLcCid,
            @RequestHeader(value = "Accept", required = false) String headerAccept,
            @RequestHeader(value = "Date", required = false) String date,
            WebRequest webRequest
    ) throws Exception {

        String timeKey = logger.startTimer();

        addressSearchService.checkRequest(host, userAgent, xClientRequestId, xForwardedFor);

        if (StringUtils.isEmpty(postcode)) {
            throw new DataFabricException("POSTCODE REQUIRED", "", "PCA");
        }

        String destination = "v4/utility-api/postcodes/" + postcode + "/addresses";
        String lccid = logger.log(moduleName, Level.INFO, null,
                "REQUEST", xLcCid, xClientRequestId, null,
                null, xForwardedFor, destination,
                postcode, (xLcCid == null ? true : false));

        String response = addressSearchService.searchAddress(postcode, xClientRequestId, lccid);

        logger.log(moduleName, Level.INFO, null, "REPONSE", xLcCid,
                xClientRequestId, null, logger.stopTimer(timeKey),
                destination, xForwardedFor, "201");

        HttpHeaders headers = addressSearchService.getResponseHeaders(xClientRequestId);

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }
}