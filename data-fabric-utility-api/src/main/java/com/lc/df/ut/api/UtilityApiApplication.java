package com.lc.df.ut.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashMap;


@Controller
@SpringBootApplication
@EnableSwagger2
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.lc.df", "com.lc.df.logging"})
public class UtilityApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {

        //loadConfiguration();
        SpringApplication.run(UtilityApiApplication.class, args);
    }

    @Bean
    public Docket newsApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("utility-api")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.ant("/v4/utility-api/**"))
                .build();
    }

    private ApiInfo apiInfo() {

return new ApiInfoBuilder()
        .title("Ladbrokes Coral API")
        .description(
                "Our API (Application Programming Interface) enables you to integrate with us and build "
                        + "applications (referred to as apps henceforth) that leverage Ladbrokes-Coral functionality "
                        + "and data. Your app might be running in a Web browser, or on a server, smartphone, tablet,"
                        + " laptop, smart TV, games console, or numerous other platforms and devices that we haven’t "
                        + "even conceived of yet.\n"
                        + "The Ladbrokes-Coral API is HTTP based, so it can be used from any application or device "
                        + "with an HTTP library. We follow a (mostly) RESTful model on our API, and we offer JSON "
                        + "or JSONP as data formats.")
        .termsOfServiceUrl("http://www-03.ibm.com/software/sla/sladb.nsf/sla/bm?Open")
        .license("Ladbrokes-Coral License Version 2.0")
        .licenseUrl("https://secure.gamblingcommission.gov.uk/gccustomweb/PublicRegister/PRSearch" +
                ".aspx?ExternalAccountId=1611")
        .version("4.0")
        .build();
    }

    private static void loadConfiguration() {

        HashMap<String, String> settings = new HashMap<String, String>();
        settings.put("CUST_MODULE_BRAND", "module.brand");
        settings.put("CUST_MODULE_DIGITAL", "module.digital");
        settings.put("CUST_MODULE_ENV", "module.environment");
        settings.put("CUST_KAFKA_PRODUCER", "kafka.servers.producer");

        for (String setting : settings.keySet()) {
            String item = System.getenv(setting);

            if (item != null) {
                System.out.println("Loading environment variable: " + setting + " = " + item);
                System.setProperty(settings.get(setting), item);
            }
        }
    }
}