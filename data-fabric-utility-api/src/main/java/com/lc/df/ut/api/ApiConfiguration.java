package com.lc.df.ut.api;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.security.KeyStore;

@Configuration
public class ApiConfiguration {

    @Value("${keystore}")
    private String keyStore;

    @Value("${module.request.timeout}")
    private int requestTime;

    @Value("${module.response.timeout}")
    private int responseTimeout;

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {

        return new MethodValidationPostProcessor();
    }

    @Bean
    public RestTemplate utilityRestTemplate(RestTemplateBuilder builder) throws Exception {

        return new RestTemplate(clientHttpRequestFactory());
    }

    private ClientHttpRequestFactory clientHttpRequestFactory() {

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        factory.setReadTimeout(requestTime);
        factory.setConnectTimeout(responseTimeout);

        return factory;
    }

    private HttpClient getSslHttpClient(String pass, String keyStore) {

        char[] password = pass.toCharArray();
        SSLConnectionSocketFactory socketFactory = null;

        try {
            socketFactory = new SSLConnectionSocketFactory(
                    new SSLContextBuilder()
                            .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                            .loadKeyMaterial(keyStore(keyStore, password), password).build());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

        return httpClient;
    }

    private KeyStore keyStore(String file, char[] password) throws Exception {

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream in = classloader.getResourceAsStream(file);
        keyStore.load(in, password);

        return keyStore;
    }
}
