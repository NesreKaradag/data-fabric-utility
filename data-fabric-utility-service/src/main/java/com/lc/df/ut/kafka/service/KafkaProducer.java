package com.lc.df.ut.kafka.service;

import ch.qos.logback.classic.Level;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.ut.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;


public class KafkaProducer {

    @Autowired
    private DataFabricLogger logger;

    @Autowired
    private KafkaTemplate<String, Event> kafkaTemplate;

    @Value("${module.name}")
    private String moduleName;

    public void send(String topic, String key, Event message, String lccid, String xClientRequestId) {

        ListenableFuture<SendResult<String, Event>> future = kafkaTemplate.send(topic, key, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, Event>>() {

            @Override
            public void onSuccess(SendResult<String, Event> result) {
                logger.log(moduleName, Level.DEBUG, "Offset: " + result.getRecordMetadata().offset() +
                        " Key: " + result.getProducerRecord().key(), "EVENT", lccid, xClientRequestId);
            }

            @Override
            public void onFailure(Throwable ex) {
                logger.log(moduleName, Level.ERROR, "Unable to send message Ex: " + ex.toString(),
                        "EVENT", lccid, xClientRequestId);
            }
        });
    }
}
