package com.lc.df.ut.addr.service;

import com.lc.df.error.model.DataFabricException;
import org.springframework.http.HttpHeaders;

public interface AddressSearchService {

    String searchAddress(String postcode,
                         String xClientRequestId,
                         String xLcCid) throws Exception;

    HttpHeaders getResponseHeaders(String xClientRequestId);

    Object unmarshalXml(String xml, Class<?> type);

    void checkRequest(String host, String userAgent,
                      String xClientRequestId, String xForwardedFor) throws DataFabricException;

}
