package com.lc.df.ut.addr.service;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lc.df.error.model.DataFabricException;
import com.lc.df.logging.DataFabricLogger;
import com.lc.df.ut.model.APIAddressSearchErrorResponse;
import com.playtech.ims.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.net.URI;
import java.util.Arrays;

@Service
public class AddressSearchServiceImpl implements AddressSearchService {

    @Autowired
    RestTemplate utilityRestTemplate;

    @Autowired
    private DataFabricLogger logger;

    @Autowired
    AddressSearchService addressSearchService;

    @Value("${module.name}")
    private String moduleName;

    @Value("${uri.address.search.path}")
    private String addressSearchPath;

    @Value("${uri.address.search.key}")
    private String addressSearchKey;

    @Value("${uri.username.address.search}")
    private String addressSearchUsername;

    public String searchAddress(String postcode, String xClientRequestId,
                                String xLcCid) throws Exception {

        String lcruid = logger.getUUID();

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Message-Id", lcruid);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(addressSearchPath)
                .queryParam("Key", addressSearchKey)
                .queryParam("Postcode", postcode)
                .queryParam("UserName", addressSearchUsername);

        ResponseEntity<String> result = (ResponseEntity<String>) runRequest("Address Search",
                xClientRequestId, xLcCid, lcruid,
                null, builder.toUriString(), String.class, HttpMethod.GET);

        ObjectMapper mapper = new ObjectMapper();

        if (result.getBody().toString().contains("Error")) {

            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            APIAddressSearchErrorResponse[] error = mapper.readValue(result.getBody().toString(),
                    APIAddressSearchErrorResponse[].class);

            throw new DataFabricException(error[0].getErrorDescription().toUpperCase(), "", "PCA");

        }
        return result.getBody().toString();

    }

    public HttpHeaders getResponseHeaders(String xClientRequestId) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Client-Request-ID", xClientRequestId);

        return headers;
    }

    public Object unmarshalXml(String xml, Class<?> type) {

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(type);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(xml);

            return unmarshaller.unmarshal(reader);
        } catch (Exception e) {
        }

        return null;
    }

    public void checkRequest(String host, String userAgent,
                             String xClientRequestId, String xForwardedFor)
            throws DataFabricException {

        if (StringUtils.isEmpty(host)) {
            throw new DataFabricException("MISSING-REQUEST-HEADER-HOST", "", "DF");
        }

        if (StringUtils.isEmpty(userAgent)) {
            throw new DataFabricException("MISSING-REQUEST-HEADER-USER-AGENT", "", "DF");
        }

        if (StringUtils.isEmpty(xClientRequestId)) {
            throw new DataFabricException("MISSING-REQUEST-HEADER-X-CLIENT-REQUEST-ID", "", "DF");
        }

        if (StringUtils.isEmpty(xForwardedFor)) {
            throw new DataFabricException("MISSING-REQUEST-HEADER-X-FORWARDED-FOR", "", "DF");
        }
    }

    private ResponseEntity<?> runRequest(
            String type, String xClientRequestId, String xLcCid, String lcruid,
            HttpEntity<?> request, String path, Class<?> clazz, HttpMethod method) throws Exception {

        String timeKey = logger.startTimer();
        ResponseEntity<?> result = null;
        try {
            logger.log(moduleName, Level.INFO, request, "REQUEST",
                    xLcCid, xClientRequestId, lcruid, null, "SELF", path, null);

            result = utilityRestTemplate.exchange(new URI(path), method, request, clazz);

            logger.log(moduleName, Level.INFO, result.getBody(), "RESPONSE",
                    xLcCid, xClientRequestId,
                    lcruid, logger.stopTimer(timeKey), path, "SELF", "200");

        } catch (HttpStatusCodeException e) {
            Error error = (Error) addressSearchService.unmarshalXml(e.getResponseBodyAsString(), Error.class);

            logger.log(moduleName, Level.ERROR, "IMS " + type + " Request Status Error: " +
                            (error != null ? error.getCode() + " " + error.getDescription() :
                                    e.getStatusCode().toString()), "RESPONSE",
                    xLcCid, xClientRequestId, lcruid, logger.stopTimer(timeKey), path, "SELF",
                    (error != null ? error.getCode() : e.getStatusCode().toString()));

        } catch (ResourceAccessException e) {
            logger.log(moduleName, Level.ERROR,
                    "IMS " + type + " Request Timeout: " + e.getMessage(),
                    "RESPONSE", xLcCid, xClientRequestId, lcruid,
                    logger.stopTimer(timeKey), path, "SELF", "408");

            throw new DataFabricException("REQUEST-TIMEOUT", "", "IMS");
        }

        return result;
    }
}
